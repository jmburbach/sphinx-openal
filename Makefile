CFLAGS=$(shell pkg-config --cflags pocketsphinx)
LDFLAGS=$(shell pkg-config --libs pocketsphinx)
MODELDIR=$(shell pkg-config --variable=modeldir pocketsphinx)

all: sphinx_openal.c
	gcc -march=native -O2 -g -pipe sphinx_openal.c -o sphinx_openal -lopenal -DMODELDIR='"$(MODELDIR)"' $(CFLAGS) $(LDFLAGS)

clean:
	rm -f sphinx_openal
