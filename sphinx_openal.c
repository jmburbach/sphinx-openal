/*
 * Copyright (c) 2012, Jacob Burbach <jmburbach@gmail.com>
 *
 * Partially based (recognition_loop) on pocketsphinx_continuous
 * from pocketsphinx. Copyright (c) 2008 Carnegie Mellon University.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *   Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer. Redistributions in binary
 *   form must reproduce the above copyright notice, this list of conditions and
 *   the following disclaimer in the documentation and/or other materials
 *   provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF
 * THE POSSIBILITY OF SUCH DAMAGE.
 */
#if defined(__APPLE__)
#include <OpenAL/al.h>
#include <OpenAL/alc.h>
#else
#include <AL/al.h>
#include <AL/alc.h>
#endif

#include <sphinxbase/ad.h>
#include <sphinxbase/cont_ad.h>
#include <sphinxbase/cmd_ln.h>
#include <sphinxbase/err.h>
#include <pocketsphinx/pocketsphinx.h>

#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <inttypes.h>
#include <stdlib.h>
#include <signal.h>

#define MAX_DEVICES 10
#define SAMPLE_RATE 16000
#define BUFFER_SIZE 800 // 800 samples = 50ms (20hz) @ 16k

#define DEFAULT_LM_FILE "data/sphinx_openal.lm"
#define DEFAULT_DIC_FILE "data/sphinx_openal.dic"
#define DEFAULT_HMM_PATH MODELDIR"/hmm/en_US/hub4wsj_sc_8k"
#define DEFAULT_END_COMMAND "END PROGRAM"

#define sleep_ms(MS) usleep(MS*1000);


struct alc_device_name_t {
	char display_name[1024];
	char device_name[1024];
};

struct application_t {
	ALCdevice* device;
	ps_decoder_t* decoder;
	cont_ad_t* cont_ad;
	int capturing;
	char lmfile[1024];
	char dicfile[1024];
	char hmmpath[1024];
	char endcmd[1024];
};
struct application_t app;

void cleanup(void);
void start_capture(void);
void stop_capture(void);
void open_capture_device(void);
int capture_samples_available(void);
void capture_samples(int16_t* buffer, int32_t count);
int32_t ad_read_func(ad_rec_t*, int16_t*, int32_t); 
void initialize_sphinx(void);
void recognition_loop(void);
void parse_command_line(int argc, char** argv);
void interrupt_handler(int);


int main(int argc, char** argv)
{
	struct sigaction interrupt_action;
	
	memset(&app, 0, sizeof(app));
	memset(&interrupt_action, 0, sizeof(interrupt_action));

	interrupt_action.sa_handler = interrupt_handler;
	sigaction(SIGINT, &interrupt_action, NULL);

	atexit(cleanup);

	strncpy(app.lmfile, DEFAULT_LM_FILE, sizeof(app.lmfile));
	strncpy(app.dicfile, DEFAULT_DIC_FILE, sizeof(app.dicfile));
	strncpy(app.hmmpath, DEFAULT_HMM_PATH, sizeof(app.hmmpath));
	strncpy(app.endcmd, DEFAULT_END_COMMAND, sizeof(app.endcmd));

	parse_command_line(argc, argv);
	open_capture_device();
	start_capture();
	initialize_sphinx();
	recognition_loop();

	cleanup();
	exit(EXIT_SUCCESS);
}

void cleanup(void)
{
	if (app.decoder) {
		ps_free(app.decoder);
		app.decoder = NULL;
	}
	
	if (app.cont_ad) {
		cont_ad_close(app.cont_ad);
		app.cont_ad = NULL;
	}

	if (app.device) {
		if (app.capturing) {
			stop_capture();
		}
		alcCloseDevice(app.device);
		app.device = NULL;
	}
}

void interrupt_handler(int sig)
{
	struct sigaction action;
	memset(&action, 0, sizeof(action));
	action.sa_handler = SIG_DFL;
	sigaction(sig, &action, NULL);

	cleanup();
	raise(sig);
}

void parse_command_line(int argc, char** argv)
{
	int opt;
	while ((opt = getopt(argc, argv, "hl:d:e:")) != -1) {
		switch (opt) {
			case 'l':
				if (access(optarg, R_OK) < 0) {
					fprintf(stderr, "File specified to -l does not exist or you do not have read permission. => \"%s\"\n", optarg);
					exit(EXIT_FAILURE);
				}
				strncpy(app.lmfile, optarg, sizeof(app.lmfile));
				break;
			case 'd':
				if (access(optarg, R_OK) < 0) {
					fprintf(stderr, "File specified to -d does not exist or you do not have read permission. => \"%s\"\n", optarg);
					exit(EXIT_FAILURE);
				}
				strncpy(app.dicfile, optarg, sizeof(app.dicfile));
				break;
			case 'e':
				strncpy(app.endcmd, optarg, sizeof(app.endcmd));
				break;
			case 'h':
			default:
				fprintf(stderr, 
						"usage: %s [OPTIONS]\n"
						"\n"
						"Options:\n"
						"  -l FILE	Specify the .lm file to use. [\""DEFAULT_LM_FILE"\"]\n"
						"  -d FILE	Specify the .dic file to use. [\""DEFAULT_DIC_FILE"\"]\n"
						"  -c COMMAND	Specify the speech command to terminate program. [\""DEFAULT_END_COMMAND"\"]\n",
						argv[0]);
				exit(EXIT_FAILURE);
		}
	}
}

void initialize_sphinx(void)
{
	cmd_ln_t* cfg;
	cont_ad_t* cont_ad;
	ps_decoder_t* ps;
	
	err_set_logfile("sphinx.log");
	
	fprintf(stderr, "Configuring sphinx, hold on...\n\n");

	cfg = cmd_ln_init(NULL, ps_args(), TRUE,
		"-hmm", app.hmmpath,
		"-lm", app.lmfile,
		"-dict", app.dicfile,
		"-debug", "0",
		NULL
	);
	if (!cfg) {
		fprintf(stderr, "failed to create sphinx config\n");
		exit(EXIT_FAILURE);
	}

	if (!(ps = ps_init(cfg))) {
		fprintf(stderr, "failed to initialize sphinx decoder\n");
		exit(EXIT_FAILURE);
	}
	app.decoder = ps;

	if (!(cont_ad = cont_ad_init(NULL, ad_read_func))) {
		fprintf(stderr, "failed to initialize voice activity detection\n");
		exit(EXIT_FAILURE);
	}
	app.cont_ad = cont_ad;

	if (cont_ad_calib(cont_ad) < 0) {
		fprintf(stderr, "failed to calibrate voice activity detection\n");
		exit(EXIT_FAILURE);
	}
}

void recognition_loop(void)
{
	int count, ts;
	int16_t buffer[BUFFER_SIZE];
	char const* hyp;
	char const* uttid;
		
	fprintf(stderr,
		"Speech recognition loop started.\n"
		"Say \"%s\" to terminate. (or interrupt with control + c)\n",
		app.endcmd);

	for (;;) {

		while ((count = cont_ad_read(app.cont_ad, buffer, BUFFER_SIZE)) == 0) {
			sleep_ms(1);
		}

		if (count < 0) {
			fprintf(stderr, "failed to read audio data\n");
			exit(EXIT_FAILURE);
		}

		if (ps_start_utt(app.decoder, NULL) < 0) {
			fprintf(stderr, "failed to start utterance\n");
			exit(EXIT_FAILURE);
		}

		ps_process_raw(app.decoder, buffer, count, FALSE, FALSE);
		ts = app.cont_ad->read_ts;

		for (;;) {
			if ((count = cont_ad_read(app.cont_ad, buffer, BUFFER_SIZE)) < 0) {
				fprintf(stderr, "failed to read audio data");
				exit(EXIT_FAILURE);
			}

			if (count == 0) {
				uint32_t elapsed = app.cont_ad->read_ts - ts;
				if (elapsed >= 8000) { // end utterance at ~0.5 seconds of silence
					break;
				}
			}
			else {
				ts = app.cont_ad->read_ts;
			}

			ps_process_raw(app.decoder, buffer, count, FALSE, FALSE);
		}

		ps_end_utt(app.decoder);
		hyp = ps_get_hyp(app.decoder, NULL, &uttid);

		fprintf(stdout, "%s\n", hyp);
		fflush(stdout);

		if (strcasecmp(hyp, app.endcmd) == 0) {
			exit(EXIT_SUCCESS);
		}
	}
}

void start_capture(void)
{
	if (app.capturing)
		return;
	alcCaptureStart(app.device);
	app.capturing = 1;
}

void stop_capture(void)
{
	if (!app.capturing)
		return;
	alcCaptureStop(app.device);
	app.capturing = 0;
}

int capture_samples_available(void)
{
	ALCsizei i;
	alcGetIntegerv(app.device, ALC_CAPTURE_SAMPLES, 1, &i);
	return (int)i;
}

void capture_samples(int16_t* buffer, int32_t count)
{
	while (capture_samples_available() < count) {
		sleep_ms(1);
	}
	alcCaptureSamples(app.device, buffer, count);
}

int32_t ad_read_func(ad_rec_t* null, int16_t* buffer, int32_t count)
{
	int nsamples = capture_samples_available();
	nsamples = nsamples > count ? count : nsamples;
	capture_samples(buffer, nsamples);
	return nsamples;
}

void open_capture_device(void)
{
	int i, count;
	const ALCchar* it;
	struct alc_device_name_t devices[MAX_DEVICES];
	ALCdevice* device;
	char buf[32];
	char* end;
	int answer;

	count = 0;
	memset(&devices, 0, sizeof(devices));

	// allow selection of default device => opens with NULL => use default or device from alsoft config
	strncpy(devices[count++].display_name,
			"Default OpenAL device", 1024);

	if (!(it = alcGetString(NULL, ALC_CAPTURE_DEVICE_SPECIFIER))) {
		fprintf(stderr, "Failed to get list of capture devices.");
		exit(EXIT_FAILURE);
	}

	while (*it && count < MAX_DEVICES) {
		strncpy(devices[count].display_name, it, 1024);
		strncpy(devices[count].device_name, it, 1024);
		++count;
		it += strlen(it) + 1;
	}
	
	fprintf(stderr, "The following capture devices are available\n\n");

	for (i = 0; i < count; ++i) {
		fprintf(stderr, "%d) %s\n", i, devices[i].display_name);
	}
	fprintf(stderr, "\n");

	for (;;) {
		fprintf(stderr, "Enter number for device to use. (q to quit): ");
		if (fgets(buf, sizeof(buf), stdin)) {
			if (buf[0] == 'q') {
				exit(0);
			}

			answer = strtol(buf, &end, 10);
			if (buf != end && answer >= 0 && answer < count) {
				break;
			}

			fprintf(stderr, "=> invalid choice, try again\n");
		}
	}

	fprintf(stderr, "=> %s\n\n", devices[answer].display_name);

	device = alcCaptureOpenDevice(devices[answer].device_name, SAMPLE_RATE, AL_FORMAT_MONO16, BUFFER_SIZE);
	if (!device) {
		fprintf(stderr, "Failed to open capture device\n");
		exit(EXIT_FAILURE);
	}

	app.device = device;
}
