#!/usr/bin/env ruby

def handle_speech_result(hyp)
	puts "I think you said => " + hyp
end

STDIN.each do |line|
	# strip newline
	line.chomp!

	# only non empty lines
	if not line.empty?
		handle_speech_result line
	end
end
