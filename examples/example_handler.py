#!/usr/bin/env python
import sys

def handle_speech_result(hyp):
	print "I think you said =>", hyp


if __name__ == "__main__":
	while True:
		line = sys.stdin.readline()
		
		# eof
		if not line:
			break

		line = line.strip()

		# ignore empty line
		if not line:
			continue

		handle_speech_result(line)
