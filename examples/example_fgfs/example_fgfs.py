import re, sys, socket

# Example controlling the 777-200ER via speech commands

NUM_MAP = {
	"ZERO": '0',
	"ONE": '1',
	"TWO": '2',
	"THREE": '3',
	"FOUR": '4',
	"FIVE": '5',
	"SIX": '6',
	"SEVEN": '7',
	"EIGHT": '8',
	"NINE": '9'
}

FLAPS_MAP = {
	"SET FLAPS ZERO": 0.0,
	"SET FLAPS ONE": 0.033,
	"SET FLAPS FIVE": 0.166,
	"SET FLAPS FIFTEEN": 0.50,
	"SET FLAPS TWENTY": 0.666,
	"SET FLAPS TWENTY FIVE": 0.833,
	"SET FLAPS THIRTY": 1.0
}

TURN_RE = re.compile(r"FLY HEADING (ZERO|ONE|TWO|THREE) (.*) (.*)$")
FLAPS_RE = re.compile(r"SET FLAPS (.*)$")
SPEED_RE = re.compile(r"SET SPEED (ONE|TWO|THREE) (.*) (.*)$")
GEAR_RE = re.compile(r"GEAR .*$")


class ExampleFGFS:

	def __init__(self):
		self.socket = None

	def connect_to_fgfs(self):
		try:
			self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
			self.socket.connect(("", 6060))
			self.socket.sendall("data\r\n");
		except socket.error:
			print "failed to connect to fgfs telnet interface"
			print "fgfs not running / not started with telnet on 6060?"
			self.socket = None
			
	def set_prop(self, prop, val):
		if self.socket:
			self.socket.sendall("set {0} {1}\r\n".format(prop, val))

	def copilot_say(self, what):
		if self.socket:
			self.socket.sendall("set /sim/messages/copilot Copilot: {0}\r\n".format(what))

	def handle_speech_result(self, hyp):
		print "handle_speech_result:", hyp

		if not self.socket:
			# try, try again
			self.connect_to_fgfs()			
			if not self.socket:
				return;
		
		match = TURN_RE.match(hyp)
		if match:
			s1, s2, s3 = match.groups()
			heading = self.convert_strings_to_number(s1, s2, s3)
			if heading > 0 and heading <= 360:
				self.set_prop("/autopilot/settings/heading-bug-deg", heading)
				self.copilot_say("Setting heading to %d..." % heading)
			return

		match = FLAPS_RE.match(hyp)
		if match:
			if FLAPS_MAP.has_key(hyp):
				self.set_prop("/controls/flight/flaps", FLAPS_MAP[hyp])
				self.copilot_say("Setting flaps to %s..." % match.groups()[0].lower())
			return

		match = SPEED_RE.match(hyp)
		if match:
			s1, s2, s3 = match.groups()
			speed = self.convert_strings_to_number(s1, s2, s3)
			self.set_prop("/autopilot/settings/target-speed-kt", speed)
			self.copilot_say("Setting speed to %d..." % speed)
			return
		
		match = GEAR_RE.match(hyp)
		if match:
			if hyp == "GEAR UP":
				self.set_prop("/controls/gear/gear-down", 0)
				self.copilot_say("Gear up...")
			elif hyp == "GEAR DOWN":
				self.set_prop("/controls/gear/gear-down", 1)
				self.copilot_say("Gear down...")
			return
	
	def convert_strings_to_number(self, *strings):
		digits = []
		for s in strings:
			digits.append(str(NUM_MAP[s]))
		number = int("".join(digits))
		return number


if __name__ == "__main__":
	handler = ExampleFGFS()
	while True:
		line = sys.stdin.readline()

		# eof
		if not line:
			break

		line = line.strip()

		# ignore empty lines
		if not line:
			continue

		handler.handle_speech_result(line)
