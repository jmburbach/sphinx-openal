import sys

digit_map = {
	0: "zero",
	1: "one",
	2: "two",
	3: "three",
	4: "four",
	5: "five",
	6: "six",
	7: "seven",
	8: "eight",
	9: "nine"
}

sentences = [
	"gear down",
	"gear up",
	"set flaps zero",
	"set flaps one",
	"set flaps five",
	"set flaps fifteen",
	"set flaps twenty",
	"set flaps twenty five",
	"set flaps thirty"
]

# generate heading sentences
for digit in range(1, 361):
	d1, d2, d3 = map(int, "%03d" % digit)
	s1 = digit_map[d1]
	s2 = digit_map[d2]
	s3 = digit_map[d3]
	sentences.append("fly heading %s %s %s" % (s1, s2, s3))


# generate speed sentences
for digit in range(100, 311):
	d1, d2, d3 = map(int, "%03d" % digit)
	s1 = digit_map[d1]
	s2 = digit_map[d2]
	s3 = digit_map[d3]
	sentences.append("set speed %s %s %s" % (s1, s2, s3))

with open("corpus.txt", "w") as corpus:
	for line in sentences:
		corpus.write("%s\n" % line)
