#!/bin/sh

handle_speech_result()
{
	echo "I think you said => $1"
}

while read -r line
do
	# handle non empty lines
	if [ -n "$line" ]
	then
		handle_speech_result "$line"
	fi
done
